OC.L10N.register(
    "email-recovery",
    {
    "Invalid Recovery Email" : "Ógildur endurheimtupóstfang",
    "Error! User email address cannot be saved as recovery email address!" : "Villa! Tölvupóstfang notanda er ekki hægt að vista sem endurheimtupóstfang!",
    "Email Recovery" : "Endurheimt með tölvupósti",
    "Email Recovery App" : "Forrit til að endurheimta með tölvupósti",
    "Recovery Email" : "Endurheimtupóstur",
    "Change Recovery Email" : "Breyta endurheimtupóstfangi",
    "Recovery Email " : "Endurheimtupóstur ",
    "Changes saved" : "Breytingar vistaðar"
},
"nplurals=2; plural=n % 10 != 1 || n % 100 == 11;");
