OC.L10N.register(
    "email-recovery",
    {
    "Invalid Recovery Email" : "Ogiltig återställningsadress",
    "Error! User email address cannot be saved as recovery email address!" : "Fel! Användares e-postadress kan inte sparas som återställningsadress!",
    "Email Recovery" : "E-poståterställning",
    "Email Recovery App" : "E-poståterställningsapp",
    "Recovery Email" : "Återställningsadress",
    "Change Recovery Email" : "Ändra återställningsadress",
    "Recovery Email " : "Återställningsadress ",
    "Changes saved" : "Ändringar sparade"
},
"nplurals=2; plural=n != 1;");
