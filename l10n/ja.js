OC.L10N.register(
    "email-recovery",
    {
    "Invalid Recovery Email" : "不正な復元用の電子メールです",
    "Error! User email address cannot be saved as recovery email address!" : "エラー。ユーザーの電子メールアドレスは復元用のメールアドレスとして保存できません！",
    "Email Recovery" : "電子メールの復元",
    "Email Recovery App" : "電子メールの復元アプリ",
    "Recovery Email" : "復元用の電子メール",
    "Change Recovery Email" : "復元用の電子メールを変更",
    "Recovery Email " : "復元用の電子メール ",
    "Changes saved" : "変更を保存しました"
},
"nplurals=1; plural=0;");
