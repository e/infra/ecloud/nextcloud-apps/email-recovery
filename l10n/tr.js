OC.L10N.register(
    "email-recovery",
    {
    "Invalid Recovery Email" : "Geçersiz Kurtarma E-postası",
    "Error! User email address cannot be saved as recovery email address!" : "Hata! Kullanıcı e-posta adresi kurtarma e-posta adresi olarak kaydedilemez!",
    "Email Recovery" : "E-posta Kurtarma",
    "Email Recovery App" : "E-posta Kurtarma Uygulaması",
    "Recovery Email" : "Kurtarma E-Postası",
    "Change Recovery Email" : "Kurtarma Adresini Değiştir",
    "Recovery Email " : "Kurtarma E-posta Adresi ",
    "Changes saved" : "Değişiklikler kaydedildi"
},
"nplurals=2; plural=n != 1;");
