OC.L10N.register(
    "email-recovery",
    {
    "Invalid Recovery Email" : "El correu electrònic de recuperació no és vàlid",
    "Error! User email address cannot be saved as recovery email address!" : "Ho sentim! El correu de l'usuari no es pot fer servir com a adreça de recuperació.",
    "Email Recovery" : "Recuperació de correu electrònic",
    "Email Recovery App" : "Aplicació de recuperació de correu electrònic",
    "Recovery Email" : "Correu electrònic de recuperació",
    "Change Recovery Email" : "Canvia el correu electrònic de recuperació",
    "Recovery Email " : "Correu electrònic de recuperació ",
    "Changes saved" : "S'han guardat els canvis"
},
"nplurals=2; plural=n != 1;");
