<?php

declare(strict_types=1);

/**
 * @copyright Copyright (c) 2020 Florent VINCENT <e.foundation>
 *
 * @author Florent VINCENT <diroots@e.email>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\EmailRecovery\AppInfo;

use OCA\EcloudAccounts\Event\BeforeUserRegisteredEvent;
use OCA\EmailRecovery\Listeners\BeforeTemplateRenderedListener;
use OCA\EmailRecovery\Listeners\BeforeUserDeletedListener;
use OCA\EmailRecovery\Listeners\BeforeUserRegisteredListener;
use OCA\EmailRecovery\Listeners\UserConfigChangedListener;
use OCA\EmailRecovery\Notification\Notifier;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\AppFramework\Http\Events\BeforeTemplateRenderedEvent;
use OCP\Notification\IManager as INotificationManager;
use OCP\User\Events\BeforeUserDeletedEvent;
use OCA\EmailRecovery\SetupChecks\EcloudAccountsIsEnableCheck;
use OCP\User\Events\UserConfigChangedEvent;

class Application extends App implements IBootstrap {
	public const APP_ID = 'email-recovery';

	public function __construct(array $urlParams = []) {
		parent::__construct(self::APP_ID, $urlParams);
	}

	public function register(IRegistrationContext $context): void {
		$context->registerEventListener(BeforeUserRegisteredEvent::class, BeforeUserRegisteredListener::class);
		$context->registerEventListener(BeforeTemplateRenderedEvent::class, BeforeTemplateRenderedListener::class);
		$context->registerEventListener(UserConfigChangedEvent::class, UserConfigChangedListener::class);
		$context->registerEventListener(BeforeUserDeletedEvent::class, BeforeUserDeletedListener::class);
		$context->registerSetupCheck(EcloudAccountsIsEnableCheck::class);
	}
	public function boot(IBootContext $context): void {
		$context->injectFn([$this, 'registerNotifier']);
	}
	public function registerNotifier(INotificationManager $manager): void {
		$manager->registerNotifierService(Notifier::class);
	}
}
