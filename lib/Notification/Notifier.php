<?php

declare(strict_types=1);
/**
 * @copyright Copyright (c) 2016, Joas Schilling <coding@schilljs.com>
 *
 * @author Joas Schilling <coding@schilljs.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\EmailRecovery\Notification;

use OCA\EmailRecovery\AppInfo\Application;
use OCA\EmailRecovery\Service\NotificationService;
use OCP\L10N\IFactory;
use OCP\Notification\INotification;
use OCP\Notification\INotifier;
use Psr\Log\LoggerInterface;

class Notifier implements INotifier {
	/** @var IFactory */
	protected $l10nFactory;
	/** @var LoggerInterface */
	private $logger;

	/** @var NotificationService */
	private $NotificationService;

	public function __construct(
		IFactory $l10nFactory,
		LoggerInterface $logger,
		NotificationService $NotificationService
	) {
		$this->l10nFactory = $l10nFactory;
		$this->logger = $logger;
		$this->NotificationService = $NotificationService;
	}

	/**
	 * Identifier of the notifier, only use [a-z0-9_]
	 *
	 * @return string
	 * @since 17.0.0
	 */
	public function getID(): string {
		return Application::APP_ID;
	}

	/**
	 * Human readable name describing the notifier
	 *
	 * @return string
	 * @since 17.0.0
	 */
	public function getName(): string {
		return $this->l10nFactory->get(Application::APP_ID)->t('Email Recovery');
	}

	/**
	 * @param INotification $notification
	 * @param string $languageCode The code of the language that should be used to prepare the notification
	 * @return INotification
	 * @throws \InvalidArgumentException When the notification was not prepared by a notifier
	 */
	public function prepare(INotification $notification, string $languageCode): INotification {
		if ($notification->getApp() !== Application::APP_ID) {
			// Not my app => throw
			throw new \InvalidArgumentException('Unknown app');
		}
		try {
			$subjectParams = $notification->getSubjectParameters();
			$messageId = $subjectParams[0];
			$username = $subjectParams[1];

			$translations = $this->NotificationService->getTranslatedSubjectAndMessage($messageId, $languageCode);
			$subject = $translations['subject'];
			$message = $translations['message'];

			$parsedSubject = $this->NotificationService->getParsedString($subject, $username);
			$subjectParameters = $parsedSubject['parameters'];
			$plainSubject = $parsedSubject['message'];
			$richSubject = $parsedSubject['richString'];

			$parsedMessage = $this->NotificationService->getParsedString($message, $username);
			$messageParameters = $parsedMessage['parameters'];
			$plainMessage = $parsedMessage['message'];
			$richMessage = $parsedMessage['richString'];

			$notification->setParsedSubject($plainSubject)->setRichSubject($richSubject, $subjectParameters);
			$notification->setParsedMessage($plainMessage)->setRichMessage($richMessage, $messageParameters);
			
			return $notification;
		} catch (\Exception $e) {
			throw new \InvalidArgumentException('Unknown app');
		}
	}
}
