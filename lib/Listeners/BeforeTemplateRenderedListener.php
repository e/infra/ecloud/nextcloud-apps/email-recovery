<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Listeners;

use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\AppFramework\Http\Events\BeforeTemplateRenderedEvent;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\Util;

class BeforeTemplateRenderedListener implements IEventListener {
	private $userSession;
	private $appName;
	private Util $util;
	private $recoveryEmailService;
	private $userId;
	private IInitialState $initialState;
	public function __construct($appName, $userId, Util $util, RecoveryEmailService $recoveryEmailService, IInitialState $initialState) {
		$this->appName = $appName;
		$this->userId = $userId;
		$this->util = $util;
		$this->recoveryEmailService = $recoveryEmailService;
		$this->initialState = $initialState;
	}

	public function handle(Event $event): void {
		if (!($event instanceof BeforeTemplateRenderedEvent)) {
			return;
		}
		if (($event->getResponse()->getRenderAs() === TemplateResponse::RENDER_AS_USER) && $event->isLoggedIn() && !empty($this->userId)) {
			// $recoveryEmail = $this->recoveryEmailService->getRecoveryEmail($this->userId);
			// if ($recoveryEmail === '') {
			$unverifiedRecoveryEmail = $this->recoveryEmailService->getUnverifiedRecoveryEmail($this->userId);
			$this->initialState->provideInitialState('unverifiedRecoveryEmail', $unverifiedRecoveryEmail);
			$this->util->addStyle($this->appName, 'email-recovery');
			$this->util->addScript($this->appName, $this->appName . '-email-recovery');
			//}
		}
	}
}
