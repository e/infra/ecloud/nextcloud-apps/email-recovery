<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Listeners;

use OCA\EcloudAccounts\Event\BeforeUserRegisteredEvent;
use OCA\EcloudAccounts\Exception\RecoveryEmailValidationException;
use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;

/**
 * Class BeforeUserRegisteredListener
 *
 * @package OCA\EmailRecovery\EventListener\User
 */
class BeforeUserRegisteredListener implements IEventListener {
	private RecoveryEmailService $recoveryEmailService;
	public function __construct(RecoveryEmailService $recoveryEmailService) {
		$this->recoveryEmailService = $recoveryEmailService;
	}

	/**
	 * @param Event $event
	 */
	public function handle(Event $event): void {
		if ($event instanceof BeforeUserRegisteredEvent) {
			$this->validateRecoveryEmail($event->getRecoveryEmail(), $event->getLanguage());
		}
	}

	/**
	 * @param string $recoveryEmail
	 * @param string $language
	 */
	protected function validateRecoveryEmail(string $recoveryEmail, string $language): void {
		try {
			$this->recoveryEmailService->validateRecoveryEmail($recoveryEmail, '', $language);
		} catch (\Exception $e) {
			throw new RecoveryEmailValidationException($e->getMessage());
		}
	}
}
