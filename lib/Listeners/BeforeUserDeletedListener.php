<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Listeners;

use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\User\Events\BeforeUserDeletedEvent;

class BeforeUserDeletedListener implements IEventListener {
	private $logger;
	private $recoveryEmailService;
	private $userManager;

	public function __construct(ILogger $logger, RecoveryEmailService $recoveryEmailService, IUserManager $userManager) {
		$this->logger = $logger;
		$this->recoveryEmailService = $recoveryEmailService;
		$this->userManager = $userManager;
	}

	public function handle(Event $event): void {
		if (!($event instanceof BeforeUserDeletedEvent)) {
			return;
		}
		$user = $event->getUser()->getUID();
		try {
			$userData = $this->userManager->get($user);
			$userEmailAddress = $userData->getEMailAddress();
			if (!empty($userEmailAddress)) {
				$this->recoveryEmailService->unrestrictEmail($userEmailAddress);
			}
		} catch (\Throwable $e) {
			$this->logger->error('Error in removing email from restricted list: '.$e->getMessage());
		}
	}
}
