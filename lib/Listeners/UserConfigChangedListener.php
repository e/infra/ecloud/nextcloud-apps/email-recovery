<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Listeners;

use Exception;
use OCA\EcloudAccounts\Service\LDAPConnectionService;
use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\User\Events\UserConfigChangedEvent;

class UserConfigChangedListener implements IEventListener {
	private $logger;
	private $recoveryEmailService;
	private $userManager;
	private LDAPConnectionService $LDAPConnectionService;
	private const LDAP_BACKEND_NAME = 'LDAP';

	public function __construct(ILogger $logger, RecoveryEmailService $recoveryEmailService, IUserManager $userManager, LDAPConnectionService $LDAPConnectionService) {
		$this->logger = $logger;
		$this->recoveryEmailService = $recoveryEmailService;
		$this->userManager = $userManager;
		$this->LDAPConnectionService = $LDAPConnectionService;
	}

	public function handle(Event $event): void {
		if (!($event instanceof UserConfigChangedEvent)) {
			return;
		}

		$username = $event->getUserId();

		if ($event->getKey() === 'unverified-recovery-email') {
			$newRecoveryEmail = $event->getValue();
			if ($newRecoveryEmail !== '') {
				$this->recoveryEmailService->setRecoveryEmail($username, '');
				$this->recoveryEmailService->sendVerificationEmail($username, $newRecoveryEmail);
			}
		}
		if ($event->getKey() === 'recovery-email') {
			$user = $this->userManager->get($username);
			$newRecoveryEmail = $event->getValue();

			if ($user->getBackend()->getBackendName() === self::LDAP_BACKEND_NAME) {
				$this->updateRecoveryEmailAtLDAPServer($username, $newRecoveryEmail);
			}
			try {
				$userEmailAddress = $user->getEMailAddress();
			
				if (empty($newRecoveryEmail)) {
					$this->recoveryEmailService->restrictEmail($userEmailAddress);
				} else {
					$this->recoveryEmailService->unrestrictEmail($userEmailAddress);
				}
			} catch (\Throwable $e) {
				$this->logger->error('Error in managing restricted list: '.$e->getMessage());
			}
		}
	}

	private function updateRecoveryEmailAtLDAPServer(string $username, $recoveryEmail) {
		$conn = $this->LDAPConnectionService->getLDAPConnection();
		$userDn = $this->LDAPConnectionService->username2dn($username);
		$entry = [
			'recoveryMailAddress' => $recoveryEmail
		];
		if (!ldap_modify($conn, $userDn, $entry)) {
			throw new Exception('Could not modify user entry');
		}
		$this->LDAPConnectionService->closeLDAPConnection($conn);
	}
}
