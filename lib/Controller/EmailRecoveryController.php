<?php

/**
 * @copyright Copyright (c) 2021 ECORP SAS <dev@e.email>
 *
 * @author Akhil Potukuchi <akhil@e.email>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\EmailRecovery\Controller;

use Exception;
use OCA\EmailRecovery\AppInfo\Application;
use OCA\EmailRecovery\Exception\BlacklistedEmailException;
use OCA\EmailRecovery\Exception\InvalidRecoveryEmailException;
use OCA\EmailRecovery\Exception\MurenaDomainDisallowedException;
use OCA\EmailRecovery\Exception\RecoveryEmailAlreadyFoundException;
use OCA\EmailRecovery\Exception\SameRecoveryEmailAsEmailException;
use OCA\EmailRecovery\Exception\TooManyVerificationAttemptsException;
use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\IL10N;
use OCP\ILogger;
use OCP\IRequest;
use OCP\IUserManager;
use OCP\IUserSession;
use OCP\Security\VerificationToken\InvalidTokenException;

class EmailRecoveryController extends Controller {
	private IConfig $config;
	private ILogger $logger;
	private IL10N $l;
	private IUserSession $userSession;
	private RecoveryEmailService $recoveryEmailService;
	private IUserManager $userManager;

	public function __construct(
		string $appName,
		IRequest             $request,
		IConfig              $config,
		ILogger              $logger,
		IL10N                $l,
		IUserSession         $userSession,
		RecoveryEmailService $recoveryEmailService,
		IUserManager $userManager,
	) {
		parent::__construct($appName, $request);
		$this->config = $config;
		$this->logger = $logger;
		$this->l = $l;
		$this->userSession = $userSession;
		$this->recoveryEmailService = $recoveryEmailService;
		$this->userManager = $userManager;
	}

	/**
	 * @NoAdminRequired
	 */

	public function getRecoveryEmail() {
		$response = new JSONResponse();
		$userId = $this->userSession->getUser()->getUID();
		$recoveryEmail = $this->config->getUserValue($userId, Application::APP_ID, 'recovery-email');
		$unverifiedRecoveryEmail = $this->config->getUserValue($userId, Application::APP_ID, 'unverified-recovery-email');
		$data = ["recoveryEmail" => $recoveryEmail, "unverifiedRecoveryEmail" => $unverifiedRecoveryEmail];
		$response->setData($data);
		return $response;
	}

	/**
	 * @NoAdminRequired
	 * @PublicPage
	 * @NoCSRFRequired
	 */
	public function verifyRecoveryEmail(string $token, string $userId): TemplateResponse {
		try {
			//decrypt email
			$email = $this->recoveryEmailService->getUnverifiedRecoveryEmail($userId);
			$ref = substr(hash('sha256', $email), 0, 8);
	
			$user = $this->userManager->get($userId);
			$verificationKey = 'verifyRecoveryMail' . $ref;
			
			$this->recoveryEmailService->verifyToken($token, $user, $verificationKey, $email);
			
			$this->recoveryEmailService->makeRecoveryEmailVerified($userId);

			$this->recoveryEmailService->deleteVerificationToken($token, $user, $verificationKey);
	
			$responseParams = [
				'title' => $this->l->t('You verified recovery email successfully.'),
				'message' => $this->l->t('You verified recovery email successfully.'),
			];
	
			return new TemplateResponse('core', 'success', $responseParams, TemplateResponse::RENDER_AS_GUEST);
		} catch (InvalidTokenException $e) {
			$error = $e->getCode() === InvalidTokenException::TOKEN_EXPIRED
				? $this->l->t('Could not verify recovery email because the token is expired')
				: $this->l->t('Could not verify recovery email because the token is invalid');
	
			$errorParams = [
				'errors' => [['error' => $error]],
			];
	
			return new TemplateResponse('core', 'error', $errorParams, TemplateResponse::RENDER_AS_GUEST);
		}
	}
	
	
	/**
	 * @NoAdminRequired
	 */

	public function setRecoveryEmail(string $recoveryEmail) {
		$userId = $this->userSession->getUser()->getUID();
		$response = new JSONResponse();
		$this->handleRecoveryEmailLogic($recoveryEmail, $userId, $response);
		return $response;
	}

	/**
	 * @NoAdminRequired
	 */
	public function resendRecoveryEmail(string $recoveryEmail) {
		$response = new JSONResponse();
		$response->setStatus(200);
		
		$userId = $this->userSession->getUser()->getUID();
		$currentRecoveryEmail = $this->recoveryEmailService->getRecoveryEmail($userId);
		if ($currentRecoveryEmail === $recoveryEmail) {
			$response->setStatus(400);
			$response->setData(['message' => $this->l->t('Recovery email is verified.')]);
			return $response;
		}
		try {
			if ($this->recoveryEmailService->limitVerficationEmail($userId, $recoveryEmail)) {
				$this->recoveryEmailService->sendVerificationEmail($userId, $recoveryEmail);
			}
		} catch (TooManyVerificationAttemptsException $e) {
			$this->logger->error('Too many verification attempts for user {userId} and email {email}: {message}', [
				'userId' => $userId,
				'email' => $recoveryEmail,
				'message' => $e->getMessage()
			]);
			$response->setStatus(429);
			$response->setData(['message' => $this->l->t('Too many verification emails.')]);
		} catch (Exception $e) {
			$this->logger->error('Error sending verification email for user {userId} and recovery email {email}: {message}', [
				'userId' => $userId,
				'email' => $recoveryEmail,
				'message' => $e->getMessage()
			]);
			$response->setStatus(500);
		}
		return $response;
	}

	private function handleRecoveryEmailLogic(string $recoveryEmail, string $userId, JSONResponse $response) {
		try {
			if ($this->recoveryEmailService->validateRecoveryEmail($recoveryEmail, $userId)) {
				$this->recoveryEmailService->updateRecoveryEmail($userId, $recoveryEmail);
				$response->setStatus(200);
			}
		} catch (Exception $e) {
			$response->setStatus(500);
			if ($e instanceof InvalidRecoveryEmailException ||
				$e instanceof SameRecoveryEmailAsEmailException ||
				$e instanceof RecoveryEmailAlreadyFoundException ||
				$e instanceof MurenaDomainDisallowedException ||
				$e instanceof BlacklistedEmailException
			) {
				$response->setStatus(400);
				$response->setData(['message' => $e->getMessage()]);
			}
		}
	}
}
