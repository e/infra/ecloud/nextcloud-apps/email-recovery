<?php

/**
 * @copyright Copyright (c) 2021 ECORP SAS <dev@e.email>
 *
 * @author Akhil Potukuchi <akhil@e.email>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\EmailRecovery\Controller;

use OCA\EmailRecovery\Exception\BlacklistedEmailException;
use OCA\EmailRecovery\Exception\InvalidRecoveryEmailException;
use OCA\EmailRecovery\Exception\SameRecoveryEmailAsEmailException;
use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\AppFramework\ApiController;
use OCP\AppFramework\Http\Response;
use OCP\ILogger;

class EmailRecoveryApiController extends ApiController {
	/** @var ILogger */
	private $logger;

	private $recoveryEmailService;

	public function __construct(
		string $appName,
		ILogger $logger,
		RecoveryEmailService $recoveryEmailService
	) {
		parent::__construct($appName, $request);
		$this->config = $config;
		$this->logger = $logger;
		$this->recoveryEmailService = $recoveryEmailService;
	}

	/**
	 * @CORS
	 * @NoCSRFRequired
	 * @PublicPage
	 */

	public function show(string $id, string $token) {
		$response = new Response;
		if (!$this->checkAppCredentials($token)) {
			$response->setStatus(401);
			return $response;
		}
		$recoveryEmail = $this->recoveryEmailService->getRecoveryEmail($id);
		return $recoveryEmail;
	}
	/**
	 * @CORS
	 * @NoCSRFRequired
	 * @PublicPage
	 */
	public function update(string $id, string $email, string $token) {
		$response = new Response;
		if (!$this->checkAppCredentials($token)) {
			$response->setStatus(401);
			return $response;
		}

		try {
			if ($this->recoveryEmailService->validateRecoveryEmail($email, $id)) {
				$this->recoveryEmailService->updateRecoveryEmail($id, $email);
				return $response;
			}
		} catch (Exception $e) {
			$response->setStatus(500);
			if ($e instanceof SameRecoveryEmailAsEmailException || $e instanceof InvalidRecoveryEmailException || $e instanceof BlacklistedEmailException) {
				$response->setStatus(400);
			}
			$this->logger->error("Error updating recovery email for user $id " . $e->getMessage());
			return $response;
		}
	}

	private function checkAppCredentials(string $token) {
		$storage_secret = $_ENV['NEXTCLOUD_EMAIL_RECOVERY_APP_SECRET'];
		return hash_equals($storage_secret, $token);
	}
}
