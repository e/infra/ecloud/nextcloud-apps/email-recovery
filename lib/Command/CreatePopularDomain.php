<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Command;

use OCA\EmailRecovery\AppInfo\Application;
use OCA\EmailRecovery\Service\DomainService;
use OCP\ILogger;
use OCP\Files\IAppData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreatePopularDomain extends Command {
	private DomainService $domainService;
	private ILogger $logger;
	private IAppData $appData;

	public function __construct(DomainService $domainService, ILogger $logger, IAppData $appData) {
		parent::__construct();
		$this->domainService = $domainService;
		$this->logger = $logger;
		$this->appData = $appData;
	}

	protected function configure() {
		$this
			->setName(Application::APP_ID.':create-popular-domains')
			->setDescription('Fetch popular email domains and create a popular_domain.json file');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		try {
			$this->domainService->updatePopularDomainsFile();
			$output->writeln('Popular domains list created successfully.');
		} catch (\Throwable $th) {
			$this->logger->error('Error while fetching popular domains. ' . $th->getMessage());
			$output->writeln('Error while fetching popular domains: ' . $th->getMessage());
			return Command::FAILURE;
		}
	
		return Command::SUCCESS;
	}
}
