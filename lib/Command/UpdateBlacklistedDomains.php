<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Command;

use OCA\EmailRecovery\AppInfo\Application;
use OCA\EmailRecovery\Service\DomainService;
use OCP\ILogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateBlacklistedDomains extends Command {
	private DomainService $domainService;
	private ILogger $logger;


	public function __construct(DomainService $domainService, ILogger $logger) {
		parent::__construct();
		$this->domainService = $domainService;
		$this->logger = $logger;
	}

	protected function configure() {
		$this->setName(Application::APP_ID.':update-blacklisted-domains')->setDescription('Update blacklisted domains');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		try {
			$this->domainService->updateBlacklistedDomains();
			$output->writeln('Updated blacklisted domains for creation.');
		} catch (\Throwable $th) {
			$this->logger->error('Error while updating blacklisted domains. ' . $th->getMessage());
			$output->writeln('Error while updating blacklisted domains. '. $th->getMessage());
		}
		return 1;
	}
}
