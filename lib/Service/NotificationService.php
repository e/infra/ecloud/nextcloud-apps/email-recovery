<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Service;

use OCA\EmailRecovery\AppInfo\Application;
use OCP\L10N\IFactory;

class NotificationService {
	/** @var IFactory */
	protected $l10nFactory;

	public function __construct(IFactory $l10nFactory) {
		$this->l10nFactory = $l10nFactory;
	}

	/**
	 * @param string $message
	 * @param string $username
	 */
	public function getParsedString(string $message, string $username) {
		$richString = $message;

		$data = $this->prepareRichString($message, $richString, $username, 'url');
		$message = $data['message'];
		$richString = $data['richString'];

		$data = $this->prepareRichString($message, $richString, $username, 'username');
		$message = $data['message'];
		$richString = $data['richString'];

		$data = $this->prepareRichString($message, $richString, $username, 'bold');
		$message = $data['message'];
		$richString = $data['richString'];

		return $this->assignVariables($message, $richString, $username);
	}
	/**
	 * @param string $message
	 * @param string $richString
	 * @param string $username
	 * @param string $type
	 */
	private function prepareRichString($message, $richString, $username, $type) {
		switch ($type) {
			case 'url':
				$richString = preg_replace('/\[(.*?)\]\((.*?)\)/', '{$1[$2]}', $message);
				break;

			case 'bold':
				preg_match_all('#\*{2}(.*?)\*{2}#', $message, $matches);
				if (sizeof($matches)) {
					foreach ($matches[1] as $match) {
						$richString = str_replace('**' . $match . '**', '{' . $match . '}', $richString);
						$message = str_replace('**' . $match . '**', $match, $message);
					}
				}
				break;

			case 'username':
				$richString = str_replace('{username}', $username, $richString);
				$message = str_replace('{username}', $username, $message);
				break;
		}
		return ['message' => $message, 'richString' => $richString];
	}
	/**
	 * @param string $message
	 * @param string $richString
	 * @param string $username
	 */
	private function assignVariables($message, $richString, $username) {
		preg_match_all('/{(.*?)}/', $richString, $matches);
		$messageParameters = [];
		if (sizeof($matches)) {
			foreach ($matches[1] as $key => $match) {
				$result = $this->checkURL($match, $richString);
				$link = $result['link'];
				$match = $result['match'];
				$richString = $result['richString'];
				$matchKey = 'key_' . $key;

				$messageParameters[$matchKey] =
					[
						'type' => ($match === 'username') ? 'user' : 'highlight',
						'id' => '',
						'name' => ($match === 'username') ? $username : $match,
						'link' => isset($link) ? $link : ''
					];

				$richString = str_replace($match, $matchKey, $richString);
			}
		}
		$placeholders = $replacements = [];
		foreach ($messageParameters as $placeholder => $parameter) {
			$placeholders[] = '{' . $placeholder . '}';
			$replacements[] = $parameter['name'];
		}
		$message = str_replace($placeholders, $replacements, $message);

		return ['message' => $message, 'richString' => $richString, 'parameters' => $messageParameters];
	}

	/**
	 * @param string $match
	 * @param string $richString
	 */
	private function checkURL($match, $richString) {
		preg_match('#\[(.*?)\]#', $match, $result);
		$link = (sizeof($result)) ? $result[1] : '';

		if (isset($link)) {
			$match = str_replace('[' . $link . ']', '', $match);
			$richString = str_replace('[' . $link . ']', '', $richString);
		}
		return ['link' => $link, 'match' => $match, 'richString' => $richString];
	}

	/**
	 * @param string $messageId
	 * @param string|null $language
	 */
	public function getTranslatedSubjectAndMessage(string $messageId, $language) {
		if (is_null($language)) {
			$language = 'en';
		}
		$l = $this->l10nFactory->get(Application::APP_ID, $language);
		return ['subject' => $l->t($messageId . '_subject'), 'message' => $l->t($messageId . '_body')];
	}
}
