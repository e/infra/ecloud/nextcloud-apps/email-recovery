<?php

declare(strict_types=1);

namespace OCA\EmailRecovery\Service;

use OCP\Files\IAppData;
use OCP\Files\NotFoundException;
use OCP\Files\SimpleFS\ISimpleFile;
use OCP\ILogger;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use OCP\IL10N;

class DomainService {
	private ILogger $logger;
	private IAppData $appData;
	private Client $httpClient;
	private string $appName;
	private IL10N $l;

	private const POPULAR_DOMAINS_URL = 'https://gist.githubusercontent.com/mavieth/418b0ba7b3525517dd85b31ee881b2ec/raw/domains.json';
	private const POPULAR_DOMAINS_FILE = 'popular_domains.json';
	private const BLACKLISTED_DOMAINS_URL = 'https://raw.githubusercontent.com/disposable/disposable-email-domains/master/domains.json';
	private const BLACKLISTED_DOMAINS_FILE = 'blacklisted_domains.json';
	private const DISPOSABLE_DOMAINS_FILE = 'disposable_domains.json';

	public function __construct(string $appName, ILogger $logger, IAppData $appData, Client $httpClient, IL10N $l) {
		$this->logger = $logger;
		$this->appData = $appData;
		$this->httpClient = $httpClient;
		$this->appName = $appName;
		$this->l = $l;
	}

	// -------------------------------
	// Public Methods
	// -------------------------------

	/**
	 * Check if an email belongs to a popular domain.
	 */
	public function isPopularDomain(string $email, IL10N $l): bool {
		$domains = $this->getDomainsFromFile(self::POPULAR_DOMAINS_FILE, $l);
		return $this->isDomainInList($email, $domains);
	}

	/**
	 * Check if an email belongs to a blacklisted domain.
	 */
	public function isBlacklistedDomain(string $email, IL10N $l): bool {
		$domains = $this->getDomainsFromFile(self::BLACKLISTED_DOMAINS_FILE, $l);
		return $this->isDomainInList($email, $domains);
	}

	/**
	 * Check if an email belongs to a custom blacklist domain.
	 */
	public function isDomainInCustomBlacklist(string $email, IL10N $l): bool {
		$domains = $this->getDomainsFromFile(self::DISPOSABLE_DOMAINS_FILE, $l);
		return $this->isDomainInList($email, $domains);
	}

	/**
	 * Update blacklisted domains by fetching from the external source.
	 */
	public function updateBlacklistedDomains(): void {
		$this->updateDomainsFromUrl(self::BLACKLISTED_DOMAINS_URL, self::BLACKLISTED_DOMAINS_FILE);
	}

	/**
	 * Add a custom disposable domain to the list.
	 */
	public function addCustomDisposableDomain(string $domain, IL10N $l, array $relatedDomains = []): void {
		// Sanitize the domain and related domains to remove control characters
		$cleanDomain = preg_replace('/[\x00-\x1F\x7F]/', '', $domain);
		$cleanRelatedDomains = array_map(fn ($relatedDomain) => preg_replace('/[\x00-\x1F\x7F]/', '', $relatedDomain), $relatedDomains);
	
		// Get existing domains from the file
		$domains = $this->getDomainsFromFile(self::DISPOSABLE_DOMAINS_FILE, $l);
	
		// Merge, deduplicate, and sanitize
		$newDomains = array_unique(array_merge($domains, [$cleanDomain], $cleanRelatedDomains));
	
		// Save the updated list to the file
		$this->saveDomainsToFile(self::DISPOSABLE_DOMAINS_FILE, $newDomains);
	}

	// -------------------------------
	// Private Helper Methods
	// -------------------------------

	/**
	 * Check if an email's domain is in the given list of domains.
	 */
	private function isDomainInList(string $email, array $domains): bool {
		if (empty($domains)) {
			return false;
		}
		$emailDomain = strtolower(explode('@', $email)[1]);
		return in_array($emailDomain, $domains, true);
	}

	/**
	 * Fetch and update domains from an external URL.
	 */
	private function updateDomainsFromUrl(string $url, string $filename): void {
		try {
			$response = $this->httpClient->request('GET', $url);
			$data = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
			$this->saveDomainsToFile($filename, $data);
		} catch (RequestException $e) {
			$this->logger->error("Failed to fetch domains from $url: " . $e->getMessage());
		} catch (\Throwable $e) {
			$this->logger->error("Unexpected error while updating domains: " . $e->getMessage());
		}
	}

	/**
	 * Get domains from a file.
	 */
private function getDomainsFromFile(string $filename, IL10N $l): array {
	try {
		// Attempt to get and read the file
		$file = $this->getDomainsFile($filename);
		$content = $file->getContent();
		if (empty($content)) {
			return [];
		}
		// Decode JSON content
		return json_decode($content, true, 512, JSON_THROW_ON_ERROR) ?? [];
	} catch (NotFoundException $e) {
		// File not found, treat as no domains configured
		$this->logger->warning("File $filename not found. Returning an empty domain list.");
		return [];
	} catch (\Throwable $e) {
		// Other errors indicate a serious issue (e.g., unreadable file)
		$this->logger->error("Error reading $filename: " . $e->getMessage());
		throw new \RuntimeException($l->t('The email could not be verified. Please try again later.'));
	}
}
	/**
	 * Save domains to a file.
	 */
	private function saveDomainsToFile(string $filename, array $domains): void {
		$file = $this->getDomainsFile($filename);
		$file->putContent(json_encode(array_values($domains)));
	}

	/**
	 * Get or create a file in AppData.
	 */
	private function getDomainsFile(string $filename): ISimpleFile {
		try {
			$folder = $this->appData->getFolder('/');
		} catch (NotFoundException $e) {
			$folder = $this->appData->newFolder('/');
		}

		if ($folder->fileExists($filename)) {
			return $folder->getFile($filename);
		}
		return $folder->newFile($filename);
	}
	
	public function updatePopularDomainsFile(): void {
		try {
			$domains = $this->fetchPopularDomains();
			$file = $this->getDomainsFile(self::POPULAR_DOMAINS_FILE);
			$file->putContent(json_encode(array_values($domains)));
			$this->logger->info('Popular domains list updated successfully.');
		} catch (\Throwable $e) {
			$this->logger->error('Error while updating popular domains file: ' . $e->getMessage());
			throw new \RuntimeException('Failed to update popular domains file.', 0, $e);
		}
	}

	/**
	 * Fetch popular email domains from an external source.
	 *
	 * @return array List of popular domains.
	 */
	private function fetchPopularDomains(): array {
		$url = self::POPULAR_DOMAINS_URL;
		try {
			$response = $this->httpClient->request('GET', $url);
			$data = json_decode($response->getBody()->getContents(), true);

			if (json_last_error() !== JSON_ERROR_NONE) {
				throw new \RuntimeException('Failed to decode JSON: ' . json_last_error_msg());
			}

			return $data;
		} catch (RequestException $e) {
			$this->logger->error('HTTP request failed: ' . $e->getMessage());
			throw new \RuntimeException('Failed to fetch popular domains.');
		} catch (\Throwable $th) {
			$this->logger->error('Unexpected error: ' . $th->getMessage());
			throw $th;
		}
	}
}
