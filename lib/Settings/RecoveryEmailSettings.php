<?php
/**
 * @copyright Copyright (c) 2021 ECORP SAS <dev@e.email>
 *
 * @author Akhil Potukuchi <akhil@e.email>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

declare(strict_types=1);


namespace OCA\EmailRecovery\Settings;

use OCA\EmailRecovery\Service\RecoveryEmailService;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\IUserSession;
use OCP\Settings\ISettings;

class RecoveryEmailSettings implements ISettings {
	private string $appName;
	private IUserSession $userSession;
	private IInitialState $initialState;
	private RecoveryEmailService $recoveryEmailService;

	public function __construct(string $appName, IUserSession $userSession, IInitialState $initialState, RecoveryEmailService $recoveryEmailService) {
		$this->appName = $appName;
		$this->userSession = $userSession;
		$this->initialState = $initialState;
		$this->recoveryEmailService = $recoveryEmailService;
	}

	public function getForm(): TemplateResponse {
		$username = $this->userSession->getUser()->getUID();
		$recoveryEmail = $this->recoveryEmailService->getRecoveryEmail($username);
		$unverifiedRecoveryEmail = $this->recoveryEmailService->getUnverifiedRecoveryEmail($username);
		$recoveryEmailVerificationStatus = $recoveryEmail != '' ? true : false;
		
		$this->initialState->provideInitialState('recoveryEmail', $recoveryEmail);
		$this->initialState->provideInitialState('recoveryEmailVerificationStatus', $recoveryEmailVerificationStatus);
		$this->initialState->provideInitialState('unverifiedRecoveryEmail', $unverifiedRecoveryEmail);
		return new TemplateResponse(
			$this->appName,
			'email_recovery_settings'
		);
	}

	public function getSection(): string {
		return 'security';
	}

	public function getPriority(): int {
		return 10;
	}
}
