# Email Recovery
- Place this app in **nextcloud/custom_apps/**
- Enable it from settings as admin user
- New field called "Recovery Email" should appear for all users in  Settings > Security > Recovery Email

![Recovery Email Setting Image](recovery-email-setting.png)
- For API endpoint authentication to work, you will have to set the environment variable `NEXTCLOUD_EMAIL_RECOVERY_APP_SECRET` on your server to some string(Recommended to set a strong password here)

## Localization

1. Download [translationtool](https://github.com/nextcloud/docker-ci/blob/master/translations/translationtool/translationtool.phar) from github.
2. Place it in your `$PATH` with executable permissions
3. Run it from the root of the project: `translationtool.phar create-pot-files email-recovery`
4. For html files, make sure that traslation strings are not directly assigned to an attribute. Use a tmp variable to call `t()` and then assign this variable to the attribute. 
4. Commit the updated `translationfiles/templates/email-recovery.pot`
5. Translate in [Weblate](https://i18n.e.foundation/projects/ecloud/email-recovery/)

## Recovery Email verification
1. Recovery Email Verification Diagram
![Recovery Verification Steps](recovery-verification-steps.png)

2. Add VerifyMail API key in config using occ
`occ config:system:set verify_mail_api_key --value='[ADD API KEY]'`

3.  Run command to set Create Popular domain

`occ email-recovery:create-popular-domains`


