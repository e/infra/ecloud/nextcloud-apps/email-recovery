<?php

/**
 * @copyright Copyright (c) 2021 ECORP SAS <dev@e.email>
 *
 * @author Akhil Potukuchi <akhil@e.email>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

return [
	'routes' => [
		['name' => 'email_recovery#set_recovery_email', 'url' => '/set_recovery_email', 'verb' => 'POST'],
		['name' => 'email_recovery#get_recovery_email', 'url' => '/get_recovery_email', 'verb' => 'GET'],
		['name' => 'email_recovery_api#preflighted_cors', 'url' => '/api/{path}',
			'verb' => 'OPTIONS', 'requirements' => ['path' => '.+']],
		['name' => 'email_recovery_api#show', 'url' => '/api/recovery-email/{id}', 'verb' => 'GET'],
		['name' => 'email_recovery_api#update', 'url' => '/api/recovery-email/{id}', 'verb' => 'PUT'],
		['name' => 'email_recovery#verify_recovery_email', 'url' => '/recovery-email/verify/{token}/{userId}', 'verb' => 'GET'],
		['name' => 'email_recovery#resend_recovery_email', 'url' => '/resend_recovery_email', 'verb' => 'POST'],
		
	]
];
