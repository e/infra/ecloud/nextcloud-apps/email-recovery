const webpackConfig = require('@nextcloud/webpack-vue-config')
const path = require('path')
module.exports = webpackConfig
module.exports = {
	...webpackConfig,
	entry: {
		'email-recovery': path.join(__dirname, 'src/email-recovery.js'),
        'main': path.join(__dirname, 'src/main.js')
	},
}

