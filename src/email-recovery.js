import { loadState } from '@nextcloud/initial-state'

const unverifiedRecoveryEmail = loadState('email-recovery', 'unverifiedRecoveryEmail')
const APPLICATION_NAME = 'email-recovery'
document.addEventListener('DOMContentLoaded', function() {
	const newDiv = createNewDiv('recovery-email-banner')
	const contentDiv = document.createElement('div')
	contentDiv.id = 'recovery-email-banner-container'
	const textNode = createTextNode(APPLICATION_NAME)
	// const link = createLinkElement(APPLICATION_NAME)

	contentDiv.appendChild(textNode)
	newDiv.appendChild(contentDiv)
	// newDiv.appendChild(link)
	insertIntoDOM(newDiv)
	// Measure the height after the element is inserted into the DOM
	const banner = document.getElementById('recovery-email-banner')
	if (banner) {
		const bannerHeight = banner.clientHeight + 'px'
		const topHeight = (banner.clientHeight + 50) + 'px'
		setTopStyle('#header', bannerHeight)
		if (document.querySelector('#content')) {
			setMarginTopAndHeight('#content', topHeight)
		} else {
			setMarginTopAndHeight('#content-vue', topHeight)
		}
		// added special case for bookmarks and passwords
		if (window.location.href.includes('apps/bookmarks/') || window.location.href.includes('apps/passwords/')) {
			const intervalId = setInterval(() => {
				const contentVue = document.querySelector('#content-vue')
				if (contentVue) {
				  setMarginTopAndHeight('#content-vue', topHeight)
				  clearInterval(intervalId)
				}
			}, 100)
		}
		setTopStyleWhenElementAvailable('#header-menu-user-menu', topHeight)
		setTopStyleWhenElementAvailable('#header-menu-notifications', topHeight)
		setTopStyle('#header-menu-unified-search', topHeight)
		banner.style.height = bannerHeight
	}

})

/**
 * Sets the 'top' style to an element once it becomes available in the DOM.
 *
 * @param {string} selector - The CSS selector for the element.
 * @param {string} topValue - The value to be set for the 'top' property.
 */

/**
 *
 * @param selector
 * @param topValue
 */
function setTopStyleWhenElementAvailable(selector, topValue) {
	// Function to check each node and apply style if it matches the selector
	/**
	 *
	 * @param node
	 */
	function checkAndApplyStyle(node) {
		if (node.nodeType === Node.ELEMENT_NODE) {
			if (node.matches(selector)) {
				node.style.top = topValue
			}

			// Check all child nodes
			node.querySelectorAll(selector).forEach(childNode => {
				childNode.style.top = topValue
			})
		}
	}

	// Set up a MutationObserver to watch for added nodes
	const observer = new MutationObserver(mutations => {
		mutations.forEach(mutation => {
			mutation.addedNodes.forEach(checkAndApplyStyle)
		})
	})

	// Start observing the document body for added nodes
	observer.observe(document.body, { childList: true, subtree: true })
}

/**
 * Sets the 'top' style property of an element.
 * The element is selected based on the provided CSS selector.
 *
 * @param {string}  selector
 * @param {string}  topValue
 */
function setTopStyle(selector, topValue) {
	const element = document.querySelector(selector)
	if (element) {
		element.style.top = topValue
	}
}

/**
 * Apply a margin-top style with !important and calculate a new height for the element.
 *
 * @param {string} selector - The CSS selector for the element.
 * @param {string} topValue - The value for the margin-top property.
 */

/**
 *
 * @param selector
 * @param topValue
 */
function setMarginTopAndHeight(selector, topValue) {
	const element = document.querySelector(selector)
	if (element) {
		element.style.cssText += `margin-top: ${topValue} !important;`
		const heightValue = `calc(100% - env(safe-area-inset-bottom) - ${topValue} - var(--body-container-margin)) !important`
		element.style.cssText += `height: ${heightValue};`
		if (window.location.href.includes('apps/passwords/')) {
			element.style.cssText += 'position:relative;'
		}
	}
}
/**
 *
 * @param className
 */
function createNewDiv(className) {
	const div = document.createElement('div')
	div.className = className
	div.id = className
	return div
}

/**
 *
 * @param appName
 */
function createTextNode(appName) {
	const p = document.createElement('p')
	let labelText = t(appName, 'Please set your recovery email address to use your email account without restrictions.')
	if (unverifiedRecoveryEmail !== '') {
		labelText = t(appName, 'Please verify your recovery email address to fully enjoy your murena.io account.')
	}
	labelText = '📢 ' + t(appName, "Murena Workspace will be back fully soon! Please <strong>read</strong> <a href='https://doc.e.foundation/backup-my-files-after-the-outage' target='_blank' style='padding-left:0px;'>this guide</a> to know more.")
	p.innerHTML = labelText
	setTimeout(() => {
		const link = p.querySelector('a') // Find the <a> tag inside <p>
		if (link) {
			link.setAttribute('target', '_blank')
			link.setAttribute('rel', 'noopener noreferrer')
		}
	}, 10)
	return p
}

/**
 *
 * @param element
 */
function insertIntoDOM(element) {
	const targetElement = document.getElementById('header')
	const parentElement = targetElement.parentNode
	parentElement.insertBefore(element, targetElement)
}
